package server

import (
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/drakonka/escalate/lib"
	"gitlab.com/drakonka/escalate/lib/reporter"
	"net/http"
	"strconv"
)

func start(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
	query := r.URL.Query()
	ticks := query.Get("ticks")
	if len(ticks) == 0 {
		ticks = "100"
	}
	t, err := strconv.Atoi(ticks)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err = w.Write([]byte(err.Error()))
		logErr(err)
		return
	}
	if t > 10000 {
		w.WriteHeader(http.StatusForbidden)
		err := errors.New("maximum ticks and passengers is 10000")
		_, err = w.Write([]byte(err.Error()))
		logErr(err)
		return
	}

	passengers := query.Get("passengers")
	if len(passengers) == 0 {
		passengers = "50"
	}
	p, err := strconv.Atoi(passengers)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err = w.Write([]byte(err.Error()))
		logErr(err)
		return
	}

	if p > 10000 {
		w.WriteHeader(http.StatusForbidden)
		err := errors.New("maximum ticks and passengers is 10000")
		_, err = w.Write([]byte(err.Error()))
		logErr(err)
		return
	}

	world, err := lib.CreateWorldWithExistingPassengers(p, t)
	if err != nil {
		writeErrorResponse(err, w)
		return
	}
	go world.Run()
	ret := fmt.Sprintf("%v", world.Id)

	w.WriteHeader(http.StatusOK)
	_, err = w.Write([]byte(ret))
	logErr(err)
}

func getState(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id := params["id"]
	tick := params["tick"]
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
	world, err := lib.GetWorld(id)
	if err != nil {
		writeErrorResponse(err, w)
		return
	}
	t, err := strconv.Atoi(tick)
	if err != nil {
		writeErrorResponse(err, w)
		return
	}
	state, err := world.GetTickState(t)
	if err != nil {
		writeErrorResponse(err, w)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(state)
	logErr(err)
}

func getAllSimulations(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
	reporter, err := reporter.NewFilesysReporter("")
	if err != nil {
		writeErrorResponse(err, w)
		return
	}
	sims, err := reporter.GetAllSimulations()
	if err != nil {
		writeErrorResponse(err, w)
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write(sims)
	logErr(err)
}

func writeErrorResponse(err error, w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	writeErr(err, w)
	return
}

func writeErr(err error, w http.ResponseWriter) {
	errMsg := fmt.Sprintf("\"%s\"", err.Error())
	_, err = w.Write([]byte(errMsg))
	logErr(err)
}

func logErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
