package escalator

import (
	"encoding/json"
	"github.com/google/uuid"
	"io/ioutil"
	"os"
	"path"
)

type passenger struct {
	ID           uuid.UUID
	Config       passengerConfig
	disembarking bool
	step         *step
}

type passengerConfig struct {
	Name                   string
	SpeedMetersPerSecond   float64
	StandingSidePreference int
	WalkingSidePreference  int
	WidthMeters            float64
}

func NewPassenger(configuration, rootPath string) (psg *passenger, err error) {
	if rootPath == "" {
		rootPath = path.Join(configsProjectRootPath(), "configs", "passengers")
		if _, err := os.Stat(rootPath); os.IsNotExist(err) {
			// path/to/whatever does not exist
			rootPath = path.Join("/etc", "escalate", "configs", "passengers")
			if _, err := os.Stat(rootPath); os.IsNotExist(err) {
				return nil, err
			}
		}
	}
	fileName := path.Join(rootPath, configuration+".json")
	_, err = os.Stat(fileName)
	if os.IsNotExist(err) {
		return nil, &ConfigNotFound{Key: configuration}
	}
	configContents, err := ioutil.ReadFile(fileName)
	if err != nil {
		panic(err)
	}
	var config passengerConfig
	err = json.Unmarshal(configContents, &config)
	if err != nil {
		return nil, &ConfigInvalid{
			Key: configuration,
			Err: err,
		}
	}

	psg = &passenger{
		ID:     uuid.New(),
		Config: config,
	}
	return psg, nil
}

func (p *passenger) tick(lowestStep *step) {
	if p.step == nil {
		if lowestStep != nil && lowestStep.canFit(p) {
			lowestStep.Occupants = append(lowestStep.Occupants, p)
			p.step = lowestStep
		} else {
			return
		}
	}
	if p.Config.SpeedMetersPerSecond == 0 {
		return
	}

	p.movePassenger()
}

func (p *passenger) movePassenger() {
	// Check where the passenger WOULD be
	posX, posY := move(p.Config.SpeedMetersPerSecond, p.step.PosX, p.step.PosY)
	nextStep := p.step.nextStep
	if nextStep.isTopStep() {
		p.disembarking = true
		return
	}
	for nextStep.isOverStep(posX, posY) && nextStep.canFit(p) {
		p.step.deleteOccupant(p)
		nextStep.Occupants = append(nextStep.Occupants, p)
		p.step = nextStep
		nextStep = p.step.nextStep
		if p.step.isTopStep() {
			p.disembarking = true
			break
		}
	}
}
