package escalator

import (
	"gitlab.com/drakonka/escalate/lib/util"
	"path"
	"path/filepath"
	"runtime"
	"testing"
)

func TestPassengerConfigLoad(t *testing.T) {
	testCases := []struct {
		name       string
		configName string
		rootPath   string
		want       error
	}{
		{"valid-config", "standing", getLivePassengersRoot(), nil},
		{"bad-test-data", "bad-test-data", getTestPassengersRoot(), &ConfigInvalid{Key: "bad-test-data"}},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := NewPassenger(tc.configName, tc.rootPath)
			testErr := util.TestErrorType(tc.want, err)
			if testErr != nil {
				t.Errorf("%s - Expected error %v, got %v", tc.name, tc.want, err)
			}
		})
	}
}

func TestPassengerMovement(t *testing.T) {
	testCases := []struct {
		name                   string
		escalatorConfigName    string
		escalatorDataRoot      string
		passengerConfigName    string
		passengerDataRoot      string
		passengerSpeedOverride float64
		wantedStepCount        int
	}{
		{"still-config", "disabled", getTestEscalatorsRoot(), "standing", getLivePassengersRoot(), 0, 0},
		{"walking-config", "disabled", getTestEscalatorsRoot(), "standing", getLivePassengersRoot(), 2, 4},
	}
	path.Join()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			esc, err := NewEscalator(tc.escalatorConfigName, tc.escalatorDataRoot)
			if err != nil {
				t.Errorf("%s - %v", tc.name, err)
				return
			}
			pass, err := NewPassenger(tc.passengerConfigName, tc.passengerDataRoot)
			if err != nil {
				t.Errorf("%s - %v", tc.name, err)
				return
			}
			pass.Config.SpeedMetersPerSecond = tc.passengerSpeedOverride
			esc.Passengers = []*passenger{pass}
			curStep := esc.Steps[0]
			wantedStep := esc.Steps[0]

			esc.Tick()
			for i := 0; i < tc.wantedStepCount; i++ {
				wantedStep = wantedStep.nextStep
			}

			var movedCount int
			for curStep != pass.step {
				movedCount++
				curStep = curStep.nextStep
			}
			if pass.step != wantedStep {
				t.Errorf("%s - Expected passenger to have moved %d steps, but moved %d", tc.name, tc.wantedStepCount, movedCount)
			}
		})
	}
}

func TestPassengerDisembark(t *testing.T) {
	testCases := []struct {
		name                   string
		escalatorConfigName    string
		escalatorDataRoot      string
		passengerConfigName    string
		passengerDataRoot      string
		passengerSpeedOverride float64
		escalatorSpeedOverride float64
		disembarkTickCount     int
	}{
		{"standing-pass-moving-esc", "disabled", getTestEscalatorsRoot(), "standing", getLivePassengersRoot(), 0, 1, 50},
		{"walking-pass-standing-esc", "disabled", getTestEscalatorsRoot(), "standing", getLivePassengersRoot(), 2, 0, 13},
		{"walking-pass-walking-esc", "disabled", getTestEscalatorsRoot(), "standing", getLivePassengersRoot(), 2, 1, 11},
	}
	path.Join()
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Make escalator
			esc, err := NewEscalator(tc.escalatorConfigName, tc.escalatorDataRoot)
			if err != nil {
				t.Errorf("%s - %v", tc.name, err)
				return
			}

			// Make passenger
			esc.Config.SpeedMetersPerSecond = tc.escalatorSpeedOverride
			pass, err := NewPassenger(tc.passengerConfigName, tc.passengerDataRoot)
			if err != nil {
				t.Errorf("%s - %v", tc.name, err)
				return
			}
			pass.Config.SpeedMetersPerSecond = tc.passengerSpeedOverride

			// Assign passenger to escalator
			esc.Passengers = []*passenger{pass}

			// Run the test
			for i := 0; i < tc.disembarkTickCount; i++ {
				var passFound bool
				for _, p := range esc.Passengers {
					if p == pass {
						passFound = true
					}
				}
				if !passFound {
					t.Errorf("%s - passenger disembarked too early, on tick %d", tc.name, i)
					return
				}
				esc.Tick()
			}
			for _, p := range esc.Passengers {
				if p == pass {
					t.Errorf("%s - passenger should be disembarked by now.", tc.name)
				}
			}
		})
	}
}

func getLivePassengersRoot() string {
	return path.Join(getDataRoot(), "configs", "passengers")
}

func getTestPassengersRoot() string {
	return path.Join(getTestDataRoot(), "passengers")
}

func getDataRoot() string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	return basepath
}

func getLiveEscalatorsRoot() string {
	return path.Join(getDataRoot(), "configs", "escalators")
}

func getTestDataRoot() string {
	return path.Join(getDataRoot(), "tests", "testdata")
}

func getTestEscalatorsRoot() string {
	return path.Join(getTestDataRoot(), "escalators")
}
