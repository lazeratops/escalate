package tests

import (
	"gitlab.com/drakonka/escalate/lib/escalator"
	"gitlab.com/drakonka/escalate/lib/util"
	"math"
	"path"
	"path/filepath"
	"runtime"
	"testing"
)

func TestEscalatorConfigLoad(t *testing.T) {
	testCases := []struct {
		name       string
		configName string
		rootPath   string
		want       error
	}{
		{"valid-config", "london-underground", getLiveEscalatorsRoot(), nil},
		{"valid-test-config", "valid-test-data", getTestEscalatorsRoot(), nil},
		{"nonexistent-config", "narnia-underground", "", &escalator.ConfigNotFound{Key: "narnia-underground"}},
		{"invalid-config", "test-data", getTestEscalatorsRoot(), &escalator.ConfigInvalid{Key: "test-data"}},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			_, err := escalator.NewEscalator(tc.configName, tc.rootPath)
			testErr := util.TestErrorType(tc.want, err)
			if testErr != nil {
				t.Errorf("%s - Expected error %v, got %v", tc.name, tc.want, err)
			}
		})
	}
}

func TestEscalatorMovement(t *testing.T) {
	testCases := []struct {
		name       string
		configName string
		wantedPosX float64
		wantedPosY float64
	}{
		{"london-underground", "london-underground", 0.530330, 0.530330},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			esc, err := escalator.NewEscalator(tc.configName, getLiveEscalatorsRoot())
			if err != nil {
				t.Errorf("%s error: %s", tc.name, err.Error())
				return
			}

			// Run for 11 ticks
			for i := 1; i <= 11; i++ {
				esc.Tick()
				// Get first step and check its position
				step := esc.Steps[0]
				stepX := math.Round(step.PosX*100) / 100
				stepY := math.Round(step.PosY*100) / 100
				wantX := math.Round(tc.wantedPosX*float64(i)*100) / 100
				wantY := math.Round(tc.wantedPosY*float64(i)*100) / 100
				if stepX != wantX || stepY != wantY {
					t.Errorf("%s - Expected position (%f, %f), got (%f, %f)", tc.name, stepX, stepY, wantX, wantY)
				}
			}
		})
	}
}

func getDataRoot() string {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	basepath = filepath.Dir(basepath)
	return basepath
}

func getLiveEscalatorsRoot() string {
	return path.Join(getDataRoot(), "configs", "escalators")
}

func getTestDataRoot() string {
	return path.Join(getDataRoot(), "tests", "testdata")
}

func getTestEscalatorsRoot() string {
	return path.Join(getTestDataRoot(), "escalators")
}
