package lib

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	escalator2 "gitlab.com/drakonka/escalate/lib/escalator"
	reporter2 "gitlab.com/drakonka/escalate/lib/reporter"
)

type World struct {
	Id             uuid.UUID
	Escalator      *escalator2.Escalator
	ElapsedSeconds int
	maxSeconds     int
	reporter       reporter2.Reporter
}

var world *World
var worlds = make(map[string]*World)

func GetWorld(id string) (*World, error) {
	world := worlds[id]
	return world, nil
}

func (w *World) GetTickState(tick int) ([]byte, error) {
	return w.reporter.GetState(tick)
}

func (w *World) GetStates(offset, limit int) ([]byte, error) {
	return w.reporter.GetStates(offset, limit)
}

func CreateWorldWithExistingPassengers(toGenerate, maxSeconds int) (*World, error) {
	world = &World{
		Id:             uuid.New(),
		ElapsedSeconds: 0,
		maxSeconds:     maxSeconds,
	}

	reporter, err := reporter2.NewFilesysReporter("")
	if err != nil {
		return nil, err
	}
	world.reporter = reporter
	escalator, err := escalator2.NewEscalator("london-underground", "")
	if err != nil {
		return nil, err
	}
	world.Escalator = escalator
	for toGenerate > 0 {
		psg, err := escalator2.NewPassenger("standing", "")
		if err != nil {
			return nil, err
		}
		escalator.AddToQueue(psg)
		toGenerate--
	}
	err = DumpState()
	if err != nil {
		fmt.Println(err)
	}
	worlds[world.Id.String()] = world
	return world, err
}

func (w *World) Run() error {
	for w.ElapsedSeconds < w.maxSeconds {
		w.Escalator.Tick()
		w.ElapsedSeconds++
		err := DumpState()
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	return nil
}

func DumpState() error {
	// Generate json
	j, err := json.Marshal(world)
	if err != nil {
		return err
	}
	err = world.reporter.AddToQueue(world.ElapsedSeconds, j)
	if err != nil {
		return err
	}
	return nil
}
