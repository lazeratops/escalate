module gitlab.com/drakonka/escalate

go 1.14

require (
	github.com/aws/aws-sdk-go v1.30.19
	github.com/bazelbuild/bazel-gazelle v0.20.0 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	golang.org/x/lint v0.0.0-20200130185559-910be7a94367 // indirect
	golang.org/x/net v0.0.0-20200501053045-e0ff5e5a1de5 // indirect
	golang.org/x/text v0.3.2 // indirect
)
